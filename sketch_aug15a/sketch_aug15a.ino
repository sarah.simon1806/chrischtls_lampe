#include <FastLED.h>
#include <IRremote.h>

//LED stuff
#define NUM_LEDS 60
#define LED_PIN 10
#define RGB_ORDER GRB
#define MAX_BRIGHTNESS 210
#define BRIGHTNESS_STEP 14
#define THRESHOLD 50000
#define MAX_VALUE 255
#define FADE_STARTINGPOINT 60
#define FADE_FINISHINGPOINT 150

//Controls
#define IR_PIN 9
#define INTERRUPT_PIN 2


//IR REMOTE KEYS
#define KEY_0 16750695
#define KEY_1 16753245
#define KEY_2 16736925
#define KEY_3 16769565
#define KEY_4 16720605
#define KEY_5 16712445
#define KEY_6 16761405
#define KEY_7 16769055
#define KEY_8 16754775
#define KEY_9 16748655
#define KEY_HASH 16756815
#define KEY_STAR 16738455
#define KEY_UP 16718055
#define KEY_DOWN 16730805
#define KEY_LEFT 16716015
#define KEY_RIGHT 16734885
#define KEY_OK 16726215

//PROGRAMS
#define PROGRAM_SPARKLE 0
#define PROGRAM_STATIC_WHITE 1
#define PROGRAM_STATIC_GREEN 2
#define PROGRAM_STATIC_ORANGE 3
#define PROGRAM_GREEN_PULSE 4
#define PROGRAM_STATIC_RED 5
#define PROGRAM_GREEN_FADE 6
#define PROGRAM_RAINBOW 7
#define PROGRAM_RAVEBOW 8
#define PROGRAM_RANDOM_COLOR 9

//COLORS
#define COLOR_GREEN 0x00DD00
#define COLOR_GREENEST 0x00FF00
#define COLOR_LIGHT_GREEN 0x75EE00
#define COLOR_DARK_GREEN 0x006F00
#define COLOR_WHITE 0xFFFFFF
#define COLOR_WARM_WHITE 0xFEF6A3
#define COLOR_ORANGE 0xCE3700
#define COLOR_AQUA 0x66CDAA
#define COLOR_BLUE 0x00EEEE
#define COLOR_BLUEST 0x0000FF
#define COLOR_RED 0xCD0000
#define COLOR_REDDEST 0xFF0000
#define COLOR_GOLD 0xFF7F00
#define COLOR_PURPLE 0x9F30FF



CRGB leds[NUM_LEDS];
long tick[NUM_LEDS];
int rainbow[NUM_LEDS];

//IR Thingis
IRrecv receiver(IR_PIN);
decode_results results;
unsigned long remote_key = 0;

//Control values
int currentProgram = PROGRAM_STATIC_WHITE;
int brightness = 210;
int fadeBrightness = 10;
int fadeStep = 10;
int rnd;
long rndtick;
uint8_t color;
uint8_t fadeHuePos = FADE_STARTINGPOINT;

bool buttonPressed = false;
bool lightsOn = true;

//Bools for loops in changing programs
bool sparklesOn = false;
bool rainbowACTIVATED = false;
bool pulseOn = false;
bool fadeOn = false;
bool ravebowOn = false;

bool finishingPoint = false;
bool valueReverse = false;

void handleInterrupt()
{
  buttonPressed = true;
}

void setup() 
{
  Serial.begin(9600);
  //Setup LEDs
  FastLED.addLeds<WS2812B, LED_PIN, RGB_ORDER>(leds, NUM_LEDS);
  FastLED.setBrightness(brightness);
  
  //Setup Infrared Receiver
  receiver.enableIRIn();

  //attatch Interrupt
  pinMode(INTERRUPT_PIN,INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN),handleInterrupt,FALLING);

  updateProgram(currentProgram); 
}

void loop() {
 //Read IR Remote
  check_Infrared();

        if(sparklesOn)
        {
          program_sparkle();
        }
        if(rainbowACTIVATED)
        {
          program_rainbow();
        } 
        if(pulseOn)
        {
          program_green_pulse();
        }
        if(fadeOn)
        {
          program_green_fade();
        }
        if(ravebowOn)
        {
          program_ravebow();
        }
        
 //check Interrupt
 if(buttonPressed)
 {
    buttonPressed = false;
    sparklesOn = false;
    rainbowACTIVATED = false;
    fadeOn = false;
    //Do Interrupt stuff
    if(currentProgram < 9)
    {
      currentProgram++;
      updateProgram(currentProgram);
    }
    else if(currentProgram == 9 && lightsOn)
    {
      lightsOn = false;
      FastLED.setBrightness(0);
    }
    else if(currentProgram == 9 && !lightsOn)
    {
      lightsOn = true;
      FastLED.setBrightness(brightness);
      currentProgram = 0;
      updateProgram(currentProgram);
    }
 }  
}

//Programs are made here:

void updateProgram(int program)
{
  delay(20);
  switch(program)
  {
    case PROGRAM_STATIC_WHITE :
      sparklesOn = false;
      rainbowACTIVATED = false;
      fadeOn = false;
      pulseOn = false;
      ravebowOn = false;
      program_white();
      break;
    case PROGRAM_STATIC_GREEN :
      sparklesOn = false;
      rainbowACTIVATED = false;
      fadeOn = false;
      pulseOn = false;
      ravebowOn = false;
      program_green();
      break;
    case PROGRAM_STATIC_ORANGE :
      sparklesOn = false;
      rainbowACTIVATED = false;
      fadeOn = false;
      pulseOn = false;
      ravebowOn = false;
      program_orange();
      break;
    case PROGRAM_STATIC_RED :
      sparklesOn = false;
      rainbowACTIVATED = false;
      fadeOn = false;
      pulseOn = false;
      ravebowOn = false;
      program_red();
      break;
    case PROGRAM_RAVEBOW :
      sparklesOn = false;
      rainbowACTIVATED = false;
      fadeOn = false;
      pulseOn = false;
      ravebowOn = true;
      break;
    case PROGRAM_RAINBOW :
      sparklesOn = false;
      rainbowACTIVATED = true;
      fadeOn = false;
      pulseOn = false;
      ravebowOn = false;
      break;
    case PROGRAM_SPARKLE :
      sparklesOn = true;
      rainbowACTIVATED = false;
      fadeOn = false;
      pulseOn = false;
      ravebowOn = false;
      break;
    case PROGRAM_GREEN_FADE :
      sparklesOn = false;
      rainbowACTIVATED = false;
      pulseOn = false;
      fadeOn = true;
      ravebowOn = false;
      break;
    case PROGRAM_GREEN_PULSE :
      sparklesOn = false;
      rainbowACTIVATED = false;
      pulseOn = true;
      fadeOn = false;
      ravebowOn = false;
      break;
    case PROGRAM_RANDOM_COLOR :
      sparklesOn = false;
      rainbowACTIVATED = false;
      fadeOn = false;
      pulseOn = false;
      ravebowOn = false;
      program_random_color();
      break;
    default: return;
  }
}

void program_white()
{
  for(int i = 0; i < NUM_LEDS; i++)
  {
    leds[i] = COLOR_WARM_WHITE;
    FastLED.show();
  }
}

void program_green()
{
  for(int i = 0; i < NUM_LEDS; i++)
  {
    leds[i] = COLOR_GREENEST;
    FastLED.show();
  }
}

void program_orange()
{
  for(int i = 0; i < NUM_LEDS; i++)
  {
    leds[i] = COLOR_ORANGE;
    FastLED.show();
  }
}

void program_red()
{
  for(int i = 0; i < NUM_LEDS; i++)
  {
    leds[i] = COLOR_REDDEST;
    FastLED.show();
  }
}

void program_sparkle()
{
    for(int i = 0; i < NUM_LEDS; i++)
    {
      rndtick = random16(1, 10000);
      tick[i] += rndtick;
      if(tick[i] >= THRESHOLD)
      {
        leds[i] = COLOR_GOLD;
        tick[i] = 0;
      } else 
      {
        leds[i] = 0x000000;
        //leds[i].fadeLightBy(MAX_BRIGHTNESS); -> make it so that not every led fades!!
      }
      FastLED.show();
    }
}

void program_random_color()
{
  color = random8();
  for(int i = 0; i < NUM_LEDS; i++)
  {
    leds[i] = CHSV(color, 255, 255);
    FastLED.show();
  }
}

void program_rainbow()
{
  static int it = 0;
  if(it < NUM_LEDS)
  {
    for(int i = 0; i < it; i++)
    {
      rainbow[i] += 15;
      if(rainbow[i] > 255)
      {
        rainbow[i] = 0;
      }
      leds[i] = CHSV(rainbow[i], 255, 255);
      FastLED.show();
    }
    it++;
  }
  for(int i = 0; i < NUM_LEDS; i++)
  {
      rainbow[i] += 15;
      if(rainbow[i] > 255)
      {
        rainbow[i] = 0;
      }
      leds[i] = CHSV(rainbow[i], 255, 255);
      FastLED.show();
  }
}

void program_green_pulse()
{
  for(int i = 0; i < NUM_LEDS; i++)
  {
    leds[i] = COLOR_GREEN;
    //leds[i].fadeLightBy(fadeBrightness);
  }
  FastLED.setBrightness(fadeBrightness);
  FastLED.show();
  fadeBrightness += fadeStep;
  if(fadeBrightness == 0 || fadeBrightness > MAX_BRIGHTNESS)
  {
    fadeStep = -fadeStep;
  }
  check_Infrared();
}

void program_ravebow()
{
  for(int i = 0; i < NUM_LEDS; i++)
  {
    leds[i] = CHSV(random8(), 255, random8());
  }
  FastLED.show(); 
}

void program_green_fade()
{
  for(int i = 0; i < NUM_LEDS; i++)
  {
    leds[i] = CHSV(fadeHuePos, 255, 255);
    if(fadeHuePos == FADE_FINISHINGPOINT)
    {
      finishingPoint = true;
    } else if(fadeHuePos == FADE_STARTINGPOINT)
    {
      finishingPoint = false;
    }
  }
  FastLED.show();
  if(finishingPoint)
  {
    fadeHuePos--;
  } else
  {
    fadeHuePos++;
  }
  check_Infrared();
}
  
void check_Infrared()
{
   delay(50);
   if(receiver.decode(&results))
  {
      remote_key = results.value;
      receiver.resume();

      switch(remote_key)
      {
          case KEY_OK:
            lightsOn = !lightsOn;
            break;
          case KEY_0:
            currentProgram = PROGRAM_SPARKLE;
            break;
          case KEY_1:
            currentProgram = PROGRAM_STATIC_WHITE;
            break;
          case KEY_2:
            currentProgram = PROGRAM_STATIC_GREEN;
            break;
          case KEY_3:
            currentProgram = PROGRAM_STATIC_ORANGE;
            break;
          case KEY_4:
            currentProgram = PROGRAM_GREEN_PULSE;
            break;
          case KEY_5:
            currentProgram = PROGRAM_STATIC_RED;
            break;
          case KEY_6:
            currentProgram = PROGRAM_GREEN_FADE;
            break;
          case KEY_7:
            currentProgram = PROGRAM_RAINBOW;
            break;
          case KEY_8:
            currentProgram = PROGRAM_RAVEBOW;
            break;
          case KEY_9:
            currentProgram = PROGRAM_RANDOM_COLOR;
            break;
              
            //Chooses a random programm above or below current program (hash is above, star is below)
          case KEY_HASH:
            rnd = random8(currentProgram,9);
            currentProgram = rnd;
            break;
          case KEY_STAR:
            rnd = random8(0, currentProgram);
            currentProgram = rnd;
            break;
              
            //BASIC CONTROL!!! 
          case KEY_RIGHT:
            if(currentProgram < 9)
            {
              currentProgram++;
            }
            else
            {
              currentProgram = 0;
            }
            break;
          case KEY_UP:
            if(brightness < MAX_BRIGHTNESS)
            {
              brightness += MAX_BRIGHTNESS/5;
            }
            break;
          case KEY_DOWN:
            if(brightness > 0)
            {
              brightness -= MAX_BRIGHTNESS/BRIGHTNESS_STEP;
            }
            break;
          case KEY_LEFT:
            if(currentProgram > 0)
              {
                currentProgram--;
              }
              else
              {
                currentProgram = 9;
              }
            break;
          default:
          if(currentProgram == PROGRAM_RAINBOW || currentProgram == PROGRAM_SPARKLE || currentProgram == PROGRAM_GREEN_FADE || currentProgram == PROGRAM_GREEN_PULSE || currentProgram == PROGRAM_RAVEBOW)
          {
            currentProgram = PROGRAM_RANDOM_COLOR;
          }
            break;
        }
        if(lightsOn)
        {
          FastLED.setBrightness(brightness); 
        }
        else
        {
          FastLED.setBrightness(0);
        }
        updateProgram(currentProgram);
  } 
}
